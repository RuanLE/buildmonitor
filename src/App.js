import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Endpoint from "./Endpoint";

class App extends Component {
  render() {

    return (<div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
          <div className="container">
              <Endpoint url="https://cognition.dev.stackworx.cloud/api/status" />
              <Endpoint url="https://ord.dev.stackworx.io/health" />
              <Endpoint url="https://api.durf.dev.stackworx.io/health" />
              <Endpoint url="https://prima.run/health" />
              <Endpoint url="https://stackworx.io" />
              <Endpoint url="https://stackworx.io/" />
          </div>


      </div>

    );
  }
}


export default App;
