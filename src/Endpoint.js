import React, { Component } from 'react';
import * as request from "request";
import './EndpointStyles.css';

class Endpoint extends Component {

    constructor(props) {
        super(props);
        this.state = {colour: "", payload: "", payloadReceived: "", code: "", uptime: null};
        this.getPayload = this.getPayload.bind(this);
    }

    setColour() {
        var context = this;

        //cant get a code, it only returns a string of some sort with the error message...
        request(context.props.url, function (error, response, data) {
            if(error) {
                context.setState(
                    {colour: "red", code: error.message}
                );
            }
            else if(response){
                context.setState(
                    {colour: "green", payloadReceived: data}
                )
            }
            else {
                console.log(error)
                context.setState(
                    {colour: "gray", code: error.message}
                );
            }
        }, function (error) {

        });

        setInterval(async () => {
            //cant get a code, it only returns a string of some sort with the error message...
            request(context.props.url, function (error, response, data) {
                if(error) {
                    context.setState(
                        {colour: "red", code: error.message}
                    );
                }
                else if(response){
                    context.setState(
                        {colour: "green", payloadReceived: data, uptime: context.state.uptime + 5}
                    )
                }
                else {
                    console.log(error)
                    context.setState(
                        {colour: "gray", code: error.message}
                    );
                }
            }, function (error) {

            });
        },300000);
    }

    componentDidMount() {
        this.setColour();
    }

    getPayload() {
        this.setState(State =>({payload: this.state.payloadReceived}));
        this.setState(State =>({uptime: 0}));
    }

    render() {
        return(
            <div className= {this.state.colour}>
                <div>
                    <button onClick={this.getPayload}>{this.props.url}</button>
                </div>
                <br/>
                {this.state.payload}
                <br/>
                {this.state.uptime}
            </div>
        )
    }
}

export default Endpoint;